# nhgym_count



## Description

I usually go to Taipei Neihu Sports Center, but since covid-19 spread around the city, I don't want to stay a place with too much people. And I found that website has a api to show how much people there are. It's helpful to check number of people immediately.

![image.png](./image.png)

```
$ python3 main.py
```
![image-1.png](./image-1.png)

## Other

Because I deploy this program by self PC, but I can't always start it. So I changed this program to iOS shortcut, and it can also execute by apple watch.

![image-2.png](./image-2.png)

![image-3.png](./image-3.png)
